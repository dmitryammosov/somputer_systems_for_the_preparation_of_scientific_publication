\select@language {russian}
\contentsline {schapter}{Preface}{3}{chapter*.2}
\select@language {english}
\contentsline {schapter}{Introduction}{4}{chapter*.3}
\select@language {english}
\select@language {russian}
\contentsline {chapter}{\numberline {1}The formula of Green-Ostrogradsky}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}The formula}{5}{section.1.1}
\contentsline {chapter}{\numberline {2}The Schrodinger equation for a hydrogen-like atom}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}The formula}{6}{section.2.1}
\contentsline {chapter}{\numberline {3}The neutron diffusion equation}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}The formula}{7}{section.3.1}
\contentsline {chapter}{\numberline {4}The diffusion-convection equation}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}The formula}{8}{section.4.1}
